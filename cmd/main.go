package main

import (
	"3soat-9-product-api/config"
	_ "3soat-9-product-api/docs"
	"3soat-9-product-api/internal/web/injection"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpclient"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpserver"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/validation"
)

// @title 3SOAT 9 Product API
// @version 1.0
// @description Projeto de cadastro de produtos
func main() {
	starter.Initialize()

	var serviceConfig config.ApplicationConfig
	err := starter.UnmarshalConfig(&serviceConfig)
	if err != nil {
		panic("error on unmarshall configs")
	}
	config.SetConfig(serviceConfig)
	httpclient.Initialize()

	sql.Initialize()

	dependencyInjection := injection.NewDependencyInjection()

	server := httpserver.Builder().
		WithConfig(starter.GetHttpServerConfig()).
		WithHealthCheck(sql.GetHealthChecker()).
		WithControllers(injection.GetAllApis(dependencyInjection)...).
		WithValidator(validation.GetEchoValidator()).
		Build()

	server.Listen()
}
