DO $$
 BEGIN
    IF EXISTS (SELECT rolname FROM pg_roles where rolname like 'product') THEN
	 SET ROLE "product";
	end if;
  end;
$$;

CREATE SCHEMA IF NOT EXISTS product;
SET STATEMENT_TIMEOUT TO '300s';
