FROM golang:1.22-alpine as base

WORKDIR /3soat-9-product-api

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download

# BUILDER
FROM base as builder

COPY . /3soat-9-product-api

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64  go build -ldflags "${LDFLAGS} -s -w" -a -o product-api /3soat-9-product-api/cmd/main.go

FROM scratch

WORKDIR /3soat-9-product-api

COPY --from=builder /3soat-9-product-api/config /3soat-9-product-api/config
COPY --from=builder /3soat-9-product-api/product-api /3soat-9-product-api


EXPOSE 8080 8081


CMD ["./product-api"]
