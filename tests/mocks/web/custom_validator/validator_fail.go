package custom_validator

import (
	"errors"
	"github.com/go-playground/validator"
)

type ValidatorFail struct {
	Validator *validator.Validate
}

func (cv *ValidatorFail) Validate(i interface{}) error {
	return errors.New("failed-validation")
}
