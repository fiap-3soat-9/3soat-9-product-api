package custom_validator

import "github.com/go-playground/validator"

type ValidatorOK struct {
	Validator *validator.Validate
}

func (cv *ValidatorOK) Validate(i interface{}) error {
	return nil
}
