package result

import (
	"3soat-9-product-api/internal/modules/ingredient/domain"
	"github.com/google/uuid"
)

type CreateIngredientResult struct {
	ID     uuid.UUID
	Name   string
	Amount int
	Type   string
}

func ToCreateIngredientResultFrom(entity domain.Ingredient) *CreateIngredientResult {
	return &CreateIngredientResult{
		ID:     entity.ID,
		Name:   entity.Name,
		Amount: entity.Amount,
		Type:   entity.Type.Name,
	}
}
