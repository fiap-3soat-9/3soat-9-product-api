package result

import (
	"3soat-9-product-api/internal/modules/ingredient/domain"
	"github.com/google/uuid"
)

type FindIngredientResult struct {
	ID     uuid.UUID
	Number int
	Name   string
	Amount int
	Type   string
}

func FromDomain(ingredient domain.Ingredient) FindIngredientResult {
	return FindIngredientResult{
		ID:     ingredient.ID,
		Number: ingredient.Number,
		Name:   ingredient.Name,
		Amount: ingredient.Amount,
		Type:   ingredient.Type.Name,
	}
}
