package database

import (
	"3soat-9-product-api/tests/mocks/db"
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"testing"
)

func TestIngredientTypeRepository(t *testing.T) {
	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find all ingredient types`, func(t *testing.T) {

		ingredientTypeRows, ingredientTypeProductGategory := createRowsIngredientType()

		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\"$").WillReturnRows(ingredientTypeRows)
		generateAssociationsMocksIngredientType(mock, ingredientTypeProductGategory)

		ingredientTypeInstance := GetIngredientTypePersistenceGateway(gormDB, gormDB, logger.Get())
		ingredientTypes, err := ingredientTypeInstance.GetAll(context.TODO())

		assert.Nil(t, err)
		assert.NotNil(t, ingredientTypes)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find all ingredient types`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\"$").WillReturnError(errors.New("error"))

		ingredientTypeInstance := GetIngredientTypePersistenceGateway(gormDB, gormDB, logger.Get())
		ingredientTypes, err := ingredientTypeInstance.GetAll(context.TODO())

		assert.NotNil(t, err)
		assert.Nil(t, ingredientTypes)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find ingredient type by name`, func(t *testing.T) {

		ingredientTypeRows, ingredientTypeProductGategory := createRowsIngredientType()

		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE name = .*$").
			WillReturnRows(ingredientTypeRows)
		generateAssociationsMocksIngredientType(mock, ingredientTypeProductGategory)

		ingredientTypeInstance := GetIngredientTypePersistenceGateway(gormDB, gormDB, logger.Get())
		ingredientType, err := ingredientTypeInstance.GetByName(context.TODO(), "test")

		assert.Nil(t, err)
		assert.NotNil(t, ingredientType)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find ingredient type by name`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE name = .*$").
			WillReturnError(errors.New("error"))

		ingredientTypes, err := ingredientTypeInstance.GetByName(context.TODO(), "type")

		assert.NotNil(t, err)
		assert.Nil(t, ingredientTypes)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}

func createRowsIngredientType() (*sqlmock.Rows, *sqlmock.Rows) {
	ingredientTypeRows := sqlmock.NewRows([]string{"name"}).AddRow("Sem alcool")
	ingredientTypeProductGategory := sqlmock.NewRows([]string{"id", "ingredient_type", "optional", "max_qtd", "product_category"}).
		AddRow(uuid.NewString(), "Sem alcool", "true", "5", "Bebida")
	return ingredientTypeRows, ingredientTypeProductGategory
}

func generateAssociationsMocksIngredientType(mock sqlmock.Sqlmock, ingredientTypeProductGategory *sqlmock.Rows) {
	mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type_product_category\" WHERE \"ingredient_type_product_category\"\\.\"ingredient_type\" = .*$").
		WillReturnRows(ingredientTypeProductGategory)
}
