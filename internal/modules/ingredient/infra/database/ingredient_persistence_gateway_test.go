package database

import (
	"3soat-9-product-api/internal/modules/ingredient/domain"
	"3soat-9-product-api/tests/mocks/db"
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"testing"
)

func TestIngredientRepository(t *testing.T) {

	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find all ingredients`, func(t *testing.T) {
		ingredientRows := sqlmock.NewRows([]string{"id", "type", "number", "name", "amount"}).
			AddRow(uuid.NewString(), "test", "1", "ingredient", "1000")
		ingredientTypeRows := sqlmock.NewRows([]string{"name"}).AddRow("test")

		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\"$").
			WillReturnRows(ingredientRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE \"ingredient_type\"\\.\"name\" = .*$").
			WillReturnRows(ingredientTypeRows)

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetAll(context.TODO())

		assert.Nil(t, err)
		assert.NotNil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive error when try to find all ingredients`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\"$").WillReturnError(errors.New("error"))

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetAll(context.TODO())

		assert.NotNil(t, err)
		assert.Nil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find ingredient by id`, func(t *testing.T) {
		id := uuid.NewString()

		ingredientRows := sqlmock.NewRows([]string{"id", "type", "number", "name", "amount"}).
			AddRow(uuid.NewString(), "test", "1", "ingredient", "1000")
		ingredientTypeRows := sqlmock.NewRows([]string{"name"}).AddRow("test")
		ingredientTypeProductGategory := sqlmock.NewRows([]string{"id", "ingredient_type", "optional", "max_qtd", "product_category"}).
			AddRow(id, "test", "true", "5", "Bebida")

		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE \"ingredient\"\\.\"id\" = .*$").
			WillReturnRows(ingredientRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE \"ingredient_type\"\\.\"name\" = .*$").
			WillReturnRows(ingredientTypeRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type_product_category\" WHERE \"ingredient_type_product_category\"\\.\"ingredient_type\" = .*$").
			WillReturnRows(ingredientTypeProductGategory)

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredient, err := ingredientInstance.GetByID(context.TODO(), uuid.MustParse(id))

		assert.Nil(t, err)
		assert.NotNil(t, ingredient)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive error when try to find ingredient by id`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE \"ingredient\"\\.\"id\" = .*$").
			WillReturnError(errors.New("error"))

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetByID(context.TODO(), uuid.New())

		assert.NotNil(t, err)
		assert.Nil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive nil when try to find nonexistent ingredient by ID`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE \"ingredient\"\\.\"id\" = .*$").
			WillReturnError(gorm.ErrRecordNotFound)

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetByID(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.Nil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find ingredient by number`, func(t *testing.T) {
		id := uuid.NewString()

		ingredientRows := sqlmock.NewRows([]string{"id", "type", "number", "name", "amount"}).
			AddRow(uuid.NewString(), "test", "1", "ingredient", "1000")
		ingredientTypeRows := sqlmock.NewRows([]string{"name"}).AddRow("test")
		ingredientTypeProductGategory := sqlmock.NewRows([]string{"id", "ingredient_type", "optional", "max_qtd", "product_category"}).
			AddRow(id, "test", "true", "5", "Bebida")

		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE number = .*$").
			WillReturnRows(ingredientRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE \"ingredient_type\"\\.\"name\" = .*$").
			WillReturnRows(ingredientTypeRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type_product_category\" WHERE \"ingredient_type_product_category\"\\.\"ingredient_type\" = .*$").
			WillReturnRows(ingredientTypeProductGategory)

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredient, err := ingredientInstance.GetByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.NotNil(t, ingredient)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive error when try to find ingredient by number`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE number = .*$").
			WillReturnError(errors.New("error"))

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetByNumber(context.TODO(), 1)

		assert.NotNil(t, err)
		assert.Nil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive nil when try to find nonexistent ingredient by number`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE number = .*$").
			WillReturnError(gorm.ErrRecordNotFound)

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.Nil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find ingredient by type`, func(t *testing.T) {
		ingredientRows := sqlmock.NewRows([]string{"id", "type", "number", "name", "amount"}).
			AddRow(uuid.NewString(), "test", "1", "ingredient", "1000")
		ingredientTypeRows := sqlmock.NewRows([]string{"name"}).AddRow("test")

		mock.ExpectQuery("^SELECT (.+) FROM \"ingredient\" JOIN ingredient_type ON ingredient_type\\.name = ingredient\\.type WHERE ingredient\\.type = .*$").
			WillReturnRows(ingredientRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE \"ingredient_type\"\\.\"name\" = .*$").
			WillReturnRows(ingredientTypeRows)

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredient, err := ingredientInstance.GetByType(context.TODO(), "type")

		assert.Nil(t, err)
		assert.NotNil(t, ingredient)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive error when try to find ingredient by type`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT (.+) FROM \"ingredient\" JOIN ingredient_type ON ingredient_type\\.name = ingredient\\.type WHERE ingredient\\.type = .*$").
			WillReturnError(errors.New("error"))

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		ingredients, err := ingredientInstance.GetByType(context.TODO(), "type")

		assert.NotNil(t, err)
		assert.Nil(t, ingredients)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should create ingredient`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"ingredient_type\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectQuery("^INSERT INTO \"ingredient\"  .*$").
			WillReturnRows(sqlmock.NewRows([]string{"number"}).AddRow(1))

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		err := ingredientInstance.Create(context.TODO(), domain.Ingredient{
			Type: domain.IngredientType{
				Name:                    "category",
				ConfigByProductCategory: nil,
			},
			ID:     uuid.New(),
			Name:   "ingredient",
			Amount: 1000,
			Number: 1,
		})

		assert.Nil(t, err)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should get error when try to create ingredient`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"ingredient_type\" (.+)$").
			WillReturnError(errors.New("error"))

		ingredientInstance := GetIngredientPersistenceGateway(gormDB, gormDB, logger.Get())
		err := ingredientInstance.Create(context.TODO(), domain.Ingredient{
			Type: domain.IngredientType{
				Name:                    "category",
				ConfigByProductCategory: nil,
			},
			ID:     uuid.New(),
			Name:   "ingredient",
			Amount: 1000,
			Number: 1,
		})

		assert.NotNil(t, err)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

}
