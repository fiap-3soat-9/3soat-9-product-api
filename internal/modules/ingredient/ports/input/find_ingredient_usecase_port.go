package input

import (
	"3soat-9-product-api/internal/modules/ingredient/usecase/result"
	"context"
)

type FindIngredientUseCasePort interface {
	FindIngredientByNumber(ctx context.Context, number int) (*result.FindIngredientResult, error)

	FindIngredientByType(ctx context.Context, ingredientType string) ([]result.FindIngredientResult, error)

	FindAllIngredients(ctx context.Context) ([]result.FindIngredientResult, error)
}
