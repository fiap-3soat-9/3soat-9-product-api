package input

import (
	"3soat-9-product-api/internal/modules/ingredient/usecase/command"
	"3soat-9-product-api/internal/modules/ingredient/usecase/result"
	"context"
)

type CreateIngredientUseCasePort interface {
	AddIngredient(ctx context.Context, command command.CreateIngredientCommand) (*result.CreateIngredientResult, error)
}
