package input

import (
	"3soat-9-product-api/internal/modules/ingredient/usecase/result"
	"context"
)

type FindIngredientTypeUseCasePort interface {
	FindAll(ctx context.Context) ([]result.IngredientTypeResult, error)
}
