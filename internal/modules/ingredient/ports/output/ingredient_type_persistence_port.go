package output

import (
	"3soat-9-product-api/internal/modules/ingredient/domain"
	"context"
)

type IngredientTypePersistencePort interface {
	GetByName(ctx context.Context, name string) (*domain.IngredientType, error)
	GetAll(ctx context.Context) ([]domain.IngredientType, error)
}
