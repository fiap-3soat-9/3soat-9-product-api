package domain

import (
	"3soat-9-product-api/internal/modules/ingredient/domain"
)

type ProductCategory struct {
	Name                    string
	AcceptCustom            bool
	ConfigByProductCategory []domain.IngredientTypeProductCategory
}
