package database

import (
	domainIngredient "3soat-9-product-api/internal/modules/ingredient/domain"
	"3soat-9-product-api/internal/modules/product/domain"
	"3soat-9-product-api/tests/mocks/db"
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"testing"
	"time"
)

func TestProductRepository(t *testing.T) {
	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find all products`, func(t *testing.T) {
		ingredientId := uuid.NewString()
		productId := uuid.NewString()

		productRows, productCategoryRows, productIngredientRows, ingredientRows, ingredientTypeRows := generateRows(productId, ingredientId)

		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE product\\.active = .*").
			WillReturnRows(productRows)
		mockAssociationsQueries(mock, productIngredientRows, ingredientRows, ingredientTypeRows, productCategoryRows)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		products, err := productPersistenceInstance.GetAll(context.TODO())

		assert.Nil(t, err)
		assert.NotNil(t, products)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find all products`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product\"").
			WillReturnError(errors.New("error"))

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetAll(context.TODO())

		assert.NotNil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find product by ID`, func(t *testing.T) {
		ingredientId := uuid.NewString()
		productId := uuid.NewString()

		productRows, productCategoryRows, productIngredientRows, ingredientRows, ingredientTypeRows := generateRows(productId, ingredientId)

		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE \"product\"\\.\"\\w+\" = .*").
			WillReturnRows(productRows)
		mockAssociationsQueries(mock, productIngredientRows, ingredientRows, ingredientTypeRows, productCategoryRows)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByID(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.NotNil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find product by ID`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE \"product\"\\.\"\\w+\" = .*").
			WillReturnError(errors.New("error"))

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByID(context.TODO(), uuid.New())

		assert.NotNil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when try id not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE \"product\"\\.\"\\w+\" = .*").
			WillReturnError(gorm.ErrRecordNotFound)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByID(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find product by number`, func(t *testing.T) {
		ingredientId := uuid.NewString()
		productId := uuid.NewString()

		productRows, productCategoryRows, productIngredientRows, ingredientRows, ingredientTypeRows := generateRows(productId, ingredientId)

		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE number = .*$").
			WillReturnRows(productRows)
		mockAssociationsQueries(mock, productIngredientRows, ingredientRows, ingredientTypeRows, productCategoryRows)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.NotNil(t, product)
	})

	t.Run(`should return error when try to find product by number`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE number = .*$").
			WillReturnError(errors.New("error"))

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByNumber(context.TODO(), 1)

		assert.NotNil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when number not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product\" WHERE number = .*$").
			WillReturnError(gorm.ErrRecordNotFound)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find product by category`, func(t *testing.T) {
		ingredientId := uuid.NewString()
		productId := uuid.NewString()

		productRows, productCategoryRows, productIngredientRows, ingredientRows, ingredientTypeRows := generateRows(productId, ingredientId)

		mock.ExpectQuery("^SELECT (.+) FROM \"product\" JOIN product_category ON product_category\\.name = product\\.category WHERE product\\.category = .*$").
			WillReturnRows(productRows)
		mockAssociationsQueries(mock, productIngredientRows, ingredientRows, ingredientTypeRows, productCategoryRows)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByCategory(context.TODO(), "category")

		assert.Nil(t, err)
		assert.NotNil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find product by category`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT (.+) FROM \"product\" JOIN product_category ON product_category\\.name = product\\.category WHERE product\\.category = .*$").
			WillReturnError(errors.New("error"))

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.GetByCategory(context.TODO(), "category")

		assert.NotNil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should create product`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"product_category\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectQuery("^INSERT INTO \"product\" .*$").
			WillReturnRows(sqlmock.NewRows([]string{"number"}).AddRow(1))
		mock.ExpectExec("^INSERT INTO \"ingredient_type\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectQuery("^INSERT INTO \"ingredient\"  .*$").
			WillReturnRows(sqlmock.NewRows([]string{"number"}).AddRow(1))
		mock.ExpectExec("^INSERT INTO \"product_ingredient\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		productPersistenceGateway := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())

		err := productPersistenceGateway.Create(context.TODO(), createProduct())
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to create product`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"product_category\" (.+)$").
			WillReturnError(errors.New("error"))

		productPersistenceGateway := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())

		err := productPersistenceGateway.Create(context.TODO(), createProduct())
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should update product`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"product_category\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectExec("^UPDATE \"product\" .*$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectExec("^INSERT INTO \"ingredient_type\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectQuery("^INSERT INTO \"ingredient\"  .*$").
			WillReturnRows(sqlmock.NewRows([]string{"number"}).AddRow(1))
		mock.ExpectExec("^INSERT INTO \"product_ingredient\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		productPersistenceGateway := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())

		err := productPersistenceGateway.Update(context.TODO(), createProduct())
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to update product`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"product_category\" (.+)$").
			WillReturnError(errors.New("error"))

		productPersistenceGateway := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())

		err := productPersistenceGateway.Update(context.TODO(), createProduct())
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should verify if product exists`, func(t *testing.T) {
		productId := uuid.NewString()

		productRows := sqlmock.NewRows([]string{"id", "name", "number", "amount", "description", "category", "menu", "img_path", "created_at", "updated_at"}).
			AddRow(productId, "test", "1", "1000", "description", "category", "true", "img_path", time.Now(), time.Now())

		mock.ExpectQuery("^SELECT (.+) FROM \"product\" .*$").
			WillReturnRows(productRows)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.CheckProductExists(context.TODO(), createProduct())

		assert.Nil(t, err)
		assert.NotNil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to verify if product exists`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT (.+) FROM \"product\" .*$").
			WillReturnError(errors.New("error"))

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.CheckProductExists(context.TODO(), createProduct())

		assert.NotNil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when product don't exist`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT (.+) FROM \"product\" .*$").
			WillReturnError(gorm.ErrRecordNotFound)

		productPersistenceInstance := GetProductPersistenceGateway(gormDB, gormDB, logger.Get())
		product, err := productPersistenceInstance.CheckProductExists(context.TODO(), createProduct())

		assert.Nil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

}

func createProduct() domain.Product {
	productId := uuid.New()

	ingredient := domainIngredient.Ingredient{
		Type: domainIngredient.IngredientType{
			Name:                    "category",
			ConfigByProductCategory: nil,
		},
		ID:     uuid.New(),
		Name:   "ingredient",
		Amount: 1000,
		Number: 1,
	}

	var ingredients []domain.ProductIngredient
	ingredients = append(ingredients, domain.ProductIngredient{
		ID:         uuid.UUID{},
		ProductId:  productId,
		Ingredient: ingredient,
		Quantity:   10,
		Amount:     1000,
	})

	return domain.Product{
		ID:          productId,
		Number:      1,
		Name:        "product",
		Amount:      10000,
		Description: "description",
		Category: domain.ProductCategory{
			Name:                    "category",
			AcceptCustom:            false,
			ConfigByProductCategory: nil,
		},
		Menu:        true,
		ImgPath:     "path",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
		Ingredients: ingredients,
		Active:      true,
	}
}

func mockAssociationsQueries(mock sqlmock.Sqlmock, productIngredientRows *sqlmock.Rows, ingredientRows *sqlmock.Rows, ingredientTypeRows *sqlmock.Rows, productCategoryRows *sqlmock.Rows) {
	mock.ExpectQuery("^SELECT \\* FROM \"product_ingredient\" WHERE \"product_ingredient\"\\.\"product_id\" = .*$").
		WillReturnRows(productIngredientRows)
	mock.ExpectQuery("^SELECT \\* FROM \"ingredient\" WHERE \"ingredient\"\\.\"id\" = .*$").
		WillReturnRows(ingredientRows)
	mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type\" WHERE \"ingredient_type\"\\.\"name\" = .*$").
		WillReturnRows(ingredientTypeRows)
	mock.ExpectQuery("^SELECT \\* FROM \"product_category\" WHERE \"product_category\"\\.\"name\" = .*$").
		WillReturnRows(productCategoryRows)
}

func generateRows(productId string, ingredientId string) (*sqlmock.Rows, *sqlmock.Rows, *sqlmock.Rows, *sqlmock.Rows, *sqlmock.Rows) {
	productRows := sqlmock.NewRows([]string{"id", "name", "number", "amount", "description", "category", "menu", "img_path", "created_at", "updated_at"}).
		AddRow(productId, "test", "1", "1000", "description", "category", "true", "img_path", time.Now(), time.Now())
	productCategoryRows := sqlmock.NewRows([]string{"name", "accept_custom"}).AddRow("category", "true")
	productIngredientRows := sqlmock.NewRows([]string{"id", "ingredient_id", "product_id", "amount", "quantity"}).
		AddRow(uuid.NewString(), ingredientId, productId, "1000", "1")
	ingredientRows := sqlmock.NewRows([]string{"id", "type", "number", "name", "amount"}).
		AddRow(ingredientId, "test", "1", "ingredient", "1000")
	ingredientTypeRows := sqlmock.NewRows([]string{"name"}).AddRow("test")
	return productRows, productCategoryRows, productIngredientRows, ingredientRows, ingredientTypeRows
}
