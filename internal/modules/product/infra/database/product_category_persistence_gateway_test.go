package database

import (
	"3soat-9-product-api/tests/mocks/db"
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"testing"
)

func TestProductCategoryRepository(t *testing.T) {
	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find all product categories`, func(t *testing.T) {
		productCategoryRows := sqlmock.NewRows([]string{"name", "accept_custom"}).AddRow("category", "true")

		mock.ExpectQuery("^SELECT \\* FROM \"product_category\"$").
			WillReturnRows(productCategoryRows)

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		products, err := productCategoryPersistenceInstance.GetAll(context.TODO())

		assert.Nil(t, err)
		assert.NotNil(t, products)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive error when try to find all product categories`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product_category\"$").
			WillReturnError(errors.New("error"))

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		products, err := productCategoryPersistenceInstance.GetAll(context.TODO())

		assert.NotNil(t, err)
		assert.Nil(t, products)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find product category by name`, func(t *testing.T) {
		productCategoryRows := sqlmock.NewRows([]string{"name", "accept_custom"}).AddRow("category", "true")

		mock.ExpectQuery("^SELECT \\* FROM \"product_category\".*?$").
			WillReturnRows(productCategoryRows)

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		product, err := productCategoryPersistenceInstance.GetByName(context.TODO(), "category")

		assert.Nil(t, err)
		assert.NotNil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive error when try to find product category by name`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product_category\".*?$").
			WillReturnError(errors.New("error"))

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		products, err := productCategoryPersistenceInstance.GetByName(context.TODO(), "category")

		assert.NotNil(t, err)
		assert.Nil(t, products)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should receive nil when try to find product category by name`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product_category\".*?$").
			WillReturnError(gorm.ErrRecordNotFound)

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		products, err := productCategoryPersistenceInstance.GetByName(context.TODO(), "category")

		assert.Nil(t, err)
		assert.Nil(t, products)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should get config by name category`, func(t *testing.T) {
		productCategoryRows := sqlmock.NewRows([]string{"name", "accept_custom"}).AddRow("category", "true")
		ingredientTypeProductGategory := sqlmock.NewRows([]string{"id", "ingredient_type", "optional", "max_qtd", "product_category"}).
			AddRow(uuid.NewString(), "type", "true", "5", "category")

		mock.ExpectQuery("^SELECT \\* FROM \"product_category\" WHERE name = .*$").
			WillReturnRows(productCategoryRows)
		mock.ExpectQuery("^SELECT \\* FROM \"ingredient_type_product_category\" WHERE \"ingredient_type_product_category\"\\.\"product_category\" = .*$").
			WillReturnRows(ingredientTypeProductGategory)

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		product, err := productCategoryPersistenceInstance.GetConfig(context.TODO(), "category")

		assert.Nil(t, err)
		assert.NotNil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to get config by name category`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product_category\" WHERE name = .*$").
			WillReturnError(errors.New("error"))

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		product, err := productCategoryPersistenceInstance.GetConfig(context.TODO(), "category")

		assert.NotNil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when config by name category not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"product_category\" WHERE name = .*$").
			WillReturnError(gorm.ErrRecordNotFound)

		productCategoryPersistenceInstance := GetProductCategoryRepository(gormDB, gormDB, logger.Get())
		product, err := productCategoryPersistenceInstance.GetConfig(context.TODO(), "category")

		assert.Nil(t, err)
		assert.Nil(t, product)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}
