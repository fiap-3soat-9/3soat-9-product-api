package usecase

import (
	"3soat-9-product-api/internal/modules/product/ports/output"
	"3soat-9-product-api/internal/modules/product/usecase/result"
	"context"
	"sync"
)

var (
	getProductCategoryUseCaseInstance *GetProductCategoryUseCase
	getProductCategoryUseCaseOnce     sync.Once
)

type GetProductCategoryUseCase struct {
	productCategoryPersistenceGateway output.ProductCategoryPersistencePort
}

func (c GetProductCategoryUseCase) FindAll(ctx context.Context) ([]result.FindProductCategoryResult, error) {
	productCategories, err := c.productCategoryPersistenceGateway.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	var categoriesResult []result.FindProductCategoryResult
	for _, productCategory := range productCategories {
		categoriesResult = append(categoriesResult, result.FindProductCategoryResult{
			Name:         productCategory.Name,
			AcceptCustom: productCategory.AcceptCustom,
		})
	}
	return categoriesResult, nil
}

func GetGetProductCategoryUseCase(productCategoryPersistenceGateway output.ProductCategoryPersistencePort) *GetProductCategoryUseCase {
	getProductCategoryUseCaseOnce.Do(func() {
		getProductCategoryUseCaseInstance = &GetProductCategoryUseCase{
			productCategoryPersistenceGateway: productCategoryPersistenceGateway,
		}
	})
	return getProductCategoryUseCaseInstance
}
