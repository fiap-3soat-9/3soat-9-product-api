package usecase

import (
	"3soat-9-product-api/internal/modules/product/ports/input"
	"3soat-9-product-api/internal/modules/product/ports/output"
	"context"
	"errors"
	"fmt"
	"sync"
)

var (
	deleteProductUseCaseInstance DeleteProductUseCase
	deleteProductUseCaseOnce     sync.Once
)

type DeleteProductUseCase struct {
	productPersistenceGateway output.ProductPersistencePort
}

func (d DeleteProductUseCase) Inactive(ctx context.Context, number int) error {
	product, err := d.productPersistenceGateway.GetByNumber(ctx, number)
	if err != nil {
		return err
	}
	if product == nil {
		return errors.New(fmt.Sprintf("product %d not found", number))
	}
	product.Active = false
	return d.productPersistenceGateway.Update(ctx, *product)
}

func GetDeleteProductUseCase(productPersistenceGateway output.ProductPersistencePort) input.DeleteProductUseCasePort {
	deleteProductUseCaseOnce.Do(func() {
		deleteProductUseCaseInstance = DeleteProductUseCase{
			productPersistenceGateway: productPersistenceGateway,
		}
	})
	return deleteProductUseCaseInstance
}
