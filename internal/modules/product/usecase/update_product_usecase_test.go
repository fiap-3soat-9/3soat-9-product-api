package usecase

import (
	ingredientDomain "3soat-9-product-api/internal/modules/ingredient/domain"
	"3soat-9-product-api/internal/modules/product/domain"
	"3soat-9-product-api/internal/modules/product/usecase/command"
	mocks2 "3soat-9-product-api/tests/mocks/modules/ingredient/ports/output"
	mocks "3soat-9-product-api/tests/mocks/modules/product/ports/output"
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"
)

func TestUpdateProductUseCase(t *testing.T) {

	t.Run(`should update product`, func(t *testing.T) {
		productPersistenceMock := mocks.NewProductPersistencePort(t)
		ingredientPersistenceMock := mocks2.NewIngredientPersistencePort(t)
		updateProductUseCase := GetUpdateProductUseCase(productPersistenceMock, ingredientPersistenceMock)

		productId := uuid.New()
		product := createProduct(productId)

		productCommand := createUpdateProductCommand()

		ingredient := ingredientDomain.Ingredient{
			ID:     uuid.New(),
			Number: 1,
			Name:   "Ingredient",
			Amount: 1000,
			Type:   ingredientDomain.IngredientType{Name: "Type"},
		}

		productPersistenceMock.On("GetByNumber", mock.Anything, 1).Return(&product, nil)
		ingredientPersistenceMock.On("GetByNumber", mock.Anything, 1).Return(&ingredient, nil)

		productPersistenceMock.On("Update", mock.Anything, mock.MatchedBy(func(c domain.Product) bool {
			return c.Name == "Edited" &&
				c.Description == "Edited"
		})).Return(nil)

		err := updateProductUseCase.UpdateProduct(context.TODO(), productCommand)

		assert.Nil(t, err)

		productPersistenceMock.AssertExpectations(t)
		productPersistenceMock.AssertCalled(t, "GetByNumber", mock.Anything, mock.Anything)
		productPersistenceMock.AssertCalled(t, "Update", mock.Anything, mock.Anything)
	})

	t.Run(`should return error when update product`, func(t *testing.T) {
		productPersistenceMock := mocks.NewProductPersistencePort(t)
		updateProductUseCase := UpdateProductUseCase{
			productPersistenceGateway: productPersistenceMock,
		}

		productId := uuid.New()
		product := createProduct(productId)

		productPersistenceMock.On("GetByNumber", mock.Anything, 0).Return(&product, nil)
		productPersistenceMock.On("Update", mock.Anything, mock.Anything).Return(errors.New("SOME_ERROR"))

		err := updateProductUseCase.UpdateProduct(context.TODO(), command.UpdateProductCommand{})

		assert.NotNil(t, err)

		productPersistenceMock.AssertExpectations(t)
		productPersistenceMock.AssertCalled(t, "GetByNumber", mock.Anything, mock.Anything)
		productPersistenceMock.AssertCalled(t, "Update", mock.Anything, mock.Anything)
	})

	t.Run(`should return error when failed to found product`, func(t *testing.T) {
		productPersistenceMock := mocks.NewProductPersistencePort(t)
		updateProductUseCase := UpdateProductUseCase{
			productPersistenceGateway: productPersistenceMock,
		}

		productPersistenceMock.On("GetByNumber", mock.Anything, 0).Return(nil, errors.New("SOME_ERROR"))

		err := updateProductUseCase.UpdateProduct(context.TODO(), command.UpdateProductCommand{})

		assert.NotNil(t, err)

		productPersistenceMock.AssertExpectations(t)
		productPersistenceMock.AssertCalled(t, "GetByNumber", mock.Anything, mock.Anything)
		productPersistenceMock.AssertNotCalled(t, "Update", mock.Anything, mock.Anything)
	})

	t.Run(`should return error when product not found`, func(t *testing.T) {
		productPersistenceMock := mocks.NewProductPersistencePort(t)
		updateProductUseCase := UpdateProductUseCase{
			productPersistenceGateway: productPersistenceMock,
		}

		productPersistenceMock.On("GetByNumber", mock.Anything, 0).Return(nil, nil)

		err := updateProductUseCase.UpdateProduct(context.TODO(), command.UpdateProductCommand{})

		assert.NotNil(t, err)

		productPersistenceMock.AssertExpectations(t)
		productPersistenceMock.AssertCalled(t, "GetByNumber", mock.Anything, mock.Anything)
		productPersistenceMock.AssertNotCalled(t, "Update", mock.Anything, mock.Anything)
	})

	t.Run(`should return error when ingredient not found`, func(t *testing.T) {
		productPersistenceMock := mocks.NewProductPersistencePort(t)
		ingredientPersistenceMock := mocks2.NewIngredientPersistencePort(t)
		updateProductUseCase := UpdateProductUseCase{
			productPersistenceGateway:    productPersistenceMock,
			ingredientPersistenceGateway: ingredientPersistenceMock,
		}

		productId := uuid.New()
		product := createProduct(productId)

		productPersistenceMock.On("GetByNumber", mock.Anything, 1).Return(&product, nil)
		ingredientPersistenceMock.On("GetByNumber", mock.Anything, 1).Return(nil, errors.New("SOME_ERROR"))

		err := updateProductUseCase.UpdateProduct(context.TODO(), createUpdateProductCommand())

		assert.NotNil(t, err)

		productPersistenceMock.AssertNotCalled(t, "Update", mock.Anything, mock.Anything)
	})
}

func createUpdateProductCommand() command.UpdateProductCommand {
	edited := "Edited"
	boolEdited := false
	productCommand := command.UpdateProductCommand{
		Number:      1,
		Name:        &edited,
		Description: &edited,
		Category:    &edited,
		Menu:        &boolEdited,
		ImgPath:     &edited,
		Ingredients: []command.Ingredient{
			{
				Number:   1,
				Quantity: 5,
			},
		},
	}
	return productCommand
}

func createProduct(productId uuid.UUID) domain.Product {
	product := domain.Product{
		ID:          productId,
		Number:      1,
		Name:        "Product",
		Amount:      4000,
		Description: "Product Description",
		Category:    domain.ProductCategory{Name: "Category"},
		Menu:        true,
		ImgPath:     "https://image.com",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
		Ingredients: []domain.ProductIngredient{
			{
				ID:        uuid.New(),
				ProductId: productId,
				Ingredient: ingredientDomain.Ingredient{
					ID:     uuid.New(),
					Number: 1,
					Name:   "Ingredient",
					Amount: 1000,
					Type:   ingredientDomain.IngredientType{Name: "Type"},
				},
				Quantity: 4,
				Amount:   4000,
			},
		},
		Active: true,
	}
	return product
}
