package input

import (
	"3soat-9-product-api/internal/modules/product/usecase/command"
	"3soat-9-product-api/internal/modules/product/usecase/result"
	"context"
)

type CreateProductUseCasePort interface {
	AddProduct(ctx context.Context, command command.CreateProductCommand) (*result.ProductResult, error)
}
