package input

import (
	"3soat-9-product-api/internal/modules/product/usecase/command"
	"context"
)

type UpdateProductUseCasePort interface {
	UpdateProduct(ctx context.Context, command command.UpdateProductCommand) error
}
