package input

import (
	"3soat-9-product-api/internal/modules/product/usecase/result"
	"context"
)

type GetProductCategoryUseCasePort interface {
	FindAll(ctx context.Context) ([]result.FindProductCategoryResult, error)
}
