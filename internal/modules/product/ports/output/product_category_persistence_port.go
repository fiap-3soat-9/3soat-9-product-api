package output

import (
	"3soat-9-product-api/internal/modules/product/domain"
	"context"
)

type ProductCategoryPersistencePort interface {
	GetAll(ctx context.Context) ([]domain.ProductCategory, error)
	GetByName(ctx context.Context, category string) (*domain.ProductCategory, error)
	GetConfig(ctx context.Context, name string) (*domain.ProductCategory, error)
}
