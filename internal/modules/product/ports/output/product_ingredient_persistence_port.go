package output

import (
	"3soat-9-product-api/internal/modules/product/domain"
	"context"
)

type ProductIngredientPersistencePort interface {
	Create(ctx context.Context, productIngredient domain.ProductIngredient) error
}
