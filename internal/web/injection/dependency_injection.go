package injection

import (
	"3soat-9-product-api/internal/web/api/rest/v1/ingredient"
	"3soat-9-product-api/internal/web/api/rest/v1/ingredienttype"
	"3soat-9-product-api/internal/web/api/rest/v1/product"
	"3soat-9-product-api/internal/web/api/rest/v1/productcategory"
	"3soat-9-product-api/internal/web/api/swagger"
	"3soat-9-product-api/internal/web/controller"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
)

type DependencyInjection struct {
	ProductApi         *product.Api
	IngredientApi      *ingredient.Api
	IngredientTypeApi  *ingredienttype.Api
	ProductCategoryApi *productcategory.Api
	Swagger            *swagger.Swagger
}

func NewDependencyInjection() DependencyInjection {

	readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")

	productUseCaseController := controller.GetProductUseCaseController(readWriteDB, readOnlyDB)
	ingredientUseCaseController := controller.GetIngredientUseCaseController(readWriteDB, readOnlyDB)

	return DependencyInjection{
		ProductApi: &product.Api{
			CreateProductUseCase:  productUseCaseController.CreateProductUseCase,
			FindProductUseCase:    productUseCaseController.FindProductUseCase,
			DeleteProductUseCase:  productUseCaseController.DeleteProductUseCase,
			UpdatedProductUseCase: productUseCaseController.UpdateProductUseCase,
		},
		IngredientApi: &ingredient.Api{
			CreateIngredientUseCase: ingredientUseCaseController.CreateIngredientUseCase,
			FindIngredientUseCase:   ingredientUseCaseController.FindIngredientUseCase,
		},
		ProductCategoryApi: &productcategory.Api{
			GetProductCategoryUseCase: productUseCaseController.GetProductCategoryUseCase,
		},
		IngredientTypeApi: &ingredienttype.Api{
			FindIngredientTypeUseCase: ingredientUseCaseController.FindIngredientTypeUseCase,
		},
		Swagger: &swagger.Swagger{},
	}
}
