package product

import (
	"3soat-9-product-api/internal/modules/product/usecase/result"
	"3soat-9-product-api/tests/mocks/modules/product/ports/input"
	"3soat-9-product-api/tests/mocks/web/custom_validator"
	"errors"
	"fmt"
	"github.com/go-playground/validator"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestProductApi(t *testing.T) {
	t.Run(`should create product`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		productResult := &result.ProductResult{
			Id:          uuid.New(),
			Name:        "product",
			Amount:      5000,
			Description: "description",
			Category:    "category",
			Menu:        true,
			ImgPath:     "path.com",
			CreatedAt:   time.Now(),
			UpdatedAt:   time.Now(),
		}

		requestBody := `{
			"name": "Lanche mais legal",
			"description": "O melhor lanche do cardápio",
			"category": "Lanche",
			"menu": true,
			"imgPath": "https://embudasartes.net/wp-content/uploads/2021/01/Captura-de-Tela-2021-01-07-a%CC%80s-01.01.31.png",
			"ingredients": [
				{
					"number": 1,
					"quantity": 1
				},
				{
					"number": 3,
					"quantity": 2
				}
			]
		}`

		createProductUseCase.On("AddProduct", mock.Anything, mock.Anything).Return(productResult, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.AddProduct(echoContext)

		createProductUseCase.AssertExpectations(t)
		createProductUseCase.AssertCalled(t, "AddProduct", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when create product validation fails`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		requestBody := `{
			"name": "Lanche mais legal",
			"description": "O melhor lanche do cardápio",
			"category": "Lanche",
			"menu": true,
			"imgPath": "https://embudasartes.net/wp-content/uploads/2021/01/Captura-de-Tela-2021-01-07-a%CC%80s-01.01.31.png",

		}`

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorFail{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.AddProduct(echoContext)

		createProductUseCase.AssertExpectations(t)
		createProductUseCase.AssertNotCalled(t, "AddProduct", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return 400 when create product when payload is invalid`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		requestBody := `{
			"name": "Lanche mais legal",
			"description": "O melhor lanche do cardápio",
			"category": "Lanche",
			"menu": true,
			"imgPath": 1,
		}`

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.AddProduct(echoContext)

		createProductUseCase.AssertExpectations(t)
		createProductUseCase.AssertNotCalled(t, "AddProduct", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should update product`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		requestBody := `{
			"name": "Lanche mais legal",
			"description": "O melhor lanche do cardápio",
			"category": "Lanche",
			"menu": true,
			"imgPath": "https://embudasartes.net/wp-content/uploads/2021/01/Captura-de-Tela-2021-01-07-a%CC%80s-01.01.31.png",
			"ingredients": [
				{
					"number": 1,
					"quantity": 1
				},
				{
					"number": 3,
					"quantity": 2
				}
			]
		}`

		number := 1

		updateProductUseCase.On("UpdateProduct", mock.Anything, mock.Anything).Return(nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/products/%d", number)
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(number))

		response := productApi.UpdateProduct(echoContext)

		updateProductUseCase.AssertExpectations(t)
		updateProductUseCase.AssertCalled(t, "UpdateProduct", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return 400 when update product with validation errors`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		requestBody := `{
			"name": "Lanche mais legal",
			"description": "O melhor lanche do cardápio",
			"category": "Lanche",
			"menu": true,
			"imgPath": "https://embudasartes.net/wp-content/uploads/2021/01/Captura-de-Tela-2021-01-07-a%CC%80s-01.01.31.png"
		}`

		number := 1

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorFail{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/products/%d", number)
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(number))

		response := productApi.UpdateProduct(echoContext)

		updateProductUseCase.AssertExpectations(t)
		updateProductUseCase.AssertNotCalled(t, "UpdateProduct", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return 400 when update product when payload is invalid`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		requestBody := `{
			"name": "Lanche mais legal",
			"description": "O melhor lanche do cardápio",
			"category": "Lanche",
			"menu": true,
			"imgPath": 2
		}`

		number := 1

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/products/%d", number)
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(number))

		response := productApi.UpdateProduct(echoContext)

		updateProductUseCase.AssertExpectations(t)
		updateProductUseCase.AssertNotCalled(t, "UpdateProduct", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should find product by number`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		ingredient := result.ProductIngredientsResult{
			Number:   1,
			Name:     "Ingredient",
			Amount:   1000,
			Type:     "ingredient",
			Quantity: 1,
		}

		productResult := &result.FindProductResult{
			Name:        "product",
			Number:      1,
			Amount:      5000,
			Description: "description",
			Category:    "category",
			Menu:        true,
			ImgPath:     "path.com",
			Ingredients: []result.ProductIngredientsResult{ingredient},
			Active:      true,
		}

		number := 1

		findProductUseCase.On("FindByNumber", mock.Anything, number).Return(productResult, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/products/%d", number)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(number))

		response := productApi.GetProductByNumber(echoContext)

		findProductUseCase.AssertExpectations(t)
		findProductUseCase.AssertCalled(t, "FindByNumber", mock.Anything, number)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when find product by invalid number`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		number := "error"

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/products/%s", number)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(number)

		response := productApi.GetProductByNumber(echoContext)

		findProductUseCase.AssertExpectations(t)
		findProductUseCase.AssertNotCalled(t, "FindByNumber", mock.Anything, number)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should find all products`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		ingredient := result.ProductIngredientsResult{
			Number:   1,
			Name:     "Ingredient",
			Amount:   1000,
			Type:     "ingredient",
			Quantity: 1,
		}

		productResult := result.FindProductResult{
			Name:        "product",
			Number:      1,
			Amount:      5000,
			Description: "description",
			Category:    "category",
			Menu:        true,
			ImgPath:     "path.com",
			Ingredients: []result.ProductIngredientsResult{ingredient},
			Active:      true,
		}

		findProductUseCase.On("FindAllProducts", mock.Anything).Return([]result.FindProductResult{productResult}, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.GetProducts(echoContext)

		findProductUseCase.AssertExpectations(t)
		findProductUseCase.AssertCalled(t, "FindAllProducts", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find products`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		findProductUseCase.On("FindAllProducts", mock.Anything).Return(nil, errors.New("some-error"))

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.GetProducts(echoContext)

		findProductUseCase.AssertExpectations(t)
		findProductUseCase.AssertCalled(t, "FindAllProducts", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should find products by category`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		ingredient := result.ProductIngredientsResult{
			Number:   1,
			Name:     "Ingredient",
			Amount:   1000,
			Type:     "ingredient",
			Quantity: 1,
		}

		productResult := result.FindProductResult{
			Name:        "product",
			Number:      1,
			Amount:      5000,
			Description: "description",
			Category:    "category",
			Menu:        true,
			ImgPath:     "path.com",
			Ingredients: []result.ProductIngredientsResult{ingredient},
			Active:      true,
		}

		findProductUseCase.On("FindByCategory", mock.Anything, "category").Return([]result.FindProductResult{productResult}, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products?category=category"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.GetProducts(echoContext)

		findProductUseCase.AssertExpectations(t)
		findProductUseCase.AssertCalled(t, "FindByCategory", mock.Anything, "category")
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find products by category`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		findProductUseCase.On("FindByCategory", mock.Anything, "category").Return(nil, errors.New("some-error"))

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products?category=category"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productApi.GetProducts(echoContext)

		findProductUseCase.AssertExpectations(t)
		findProductUseCase.AssertCalled(t, "FindByCategory", mock.Anything, "category")
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should inactivate product`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		deleteProductUseCase.On("Inactive", mock.Anything, 1).Return(nil)

		number := "1"
		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products/1"
		req, _ := http.NewRequest(http.MethodDelete, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(number)

		response := productApi.InactiveProductByNumber(echoContext)

		deleteProductUseCase.AssertExpectations(t)
		deleteProductUseCase.AssertCalled(t, "Inactive", mock.Anything, 1)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to inactivate product`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		deleteProductUseCase.On("Inactive", mock.Anything, 1).Return(errors.New("some-error"))

		number := "1"
		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products/1"
		req, _ := http.NewRequest(http.MethodDelete, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(number)

		response := productApi.InactiveProductByNumber(echoContext)

		deleteProductUseCase.AssertExpectations(t)
		deleteProductUseCase.AssertCalled(t, "Inactive", mock.Anything, 1)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should return 400 when try to inactivate product with invalid number`, func(t *testing.T) {
		createProductUseCase := mocks.NewCreateProductUseCasePort(t)
		deleteProductUseCase := mocks.NewDeleteProductUseCasePort(t)
		updateProductUseCase := mocks.NewUpdateProductUseCasePort(t)
		findProductUseCase := mocks.NewFindProductUseCasePort(t)

		productApi := Api{
			CreateProductUseCase:  createProductUseCase,
			UpdatedProductUseCase: updateProductUseCase,
			FindProductUseCase:    findProductUseCase,
			DeleteProductUseCase:  deleteProductUseCase,
		}

		number := "error"
		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/products/1"
		req, _ := http.NewRequest(http.MethodDelete, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(number)

		response := productApi.InactiveProductByNumber(echoContext)

		deleteProductUseCase.AssertExpectations(t)
		deleteProductUseCase.AssertNotCalled(t, "Inactive", mock.Anything, 1)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

}
