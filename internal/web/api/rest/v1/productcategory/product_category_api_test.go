package productcategory

import (
	"3soat-9-product-api/internal/modules/product/usecase/result"
	mocks "3soat-9-product-api/tests/mocks/modules/product/ports/input"
	"3soat-9-product-api/tests/mocks/web/custom_validator"
	"errors"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestProductCategoryApi(t *testing.T) {

	t.Run(`should find all product categories`, func(t *testing.T) {
		getProductCategoryUseCaseMock := mocks.NewGetProductCategoryUseCasePort(t)

		productCategoryApi := Api{
			GetProductCategoryUseCase: getProductCategoryUseCaseMock,
		}

		productCategoryResult := result.FindProductCategoryResult{
			Name:         "type",
			AcceptCustom: true,
		}

		getProductCategoryUseCaseMock.On("FindAll", mock.Anything).Return(
			[]result.FindProductCategoryResult{productCategoryResult},
			nil,
		)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/product-categories"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productCategoryApi.GetProductCategories(echoContext)

		getProductCategoryUseCaseMock.AssertExpectations(t)
		getProductCategoryUseCaseMock.AssertCalled(t, "FindAll", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return no content when product categories is empty`, func(t *testing.T) {
		getProductCategoryUseCaseMock := mocks.NewGetProductCategoryUseCasePort(t)

		productCategoryApi := Api{
			GetProductCategoryUseCase: getProductCategoryUseCaseMock,
		}

		getProductCategoryUseCaseMock.On("FindAll", mock.Anything).Return(nil, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/product-categories"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productCategoryApi.GetProductCategories(echoContext)

		getProductCategoryUseCaseMock.AssertExpectations(t)
		getProductCategoryUseCaseMock.AssertCalled(t, "FindAll", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusNoContent, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find all ingredient types`, func(t *testing.T) {
		getProductCategoryUseCaseMock := mocks.NewGetProductCategoryUseCasePort(t)

		productCategoryApi := Api{
			GetProductCategoryUseCase: getProductCategoryUseCaseMock,
		}

		getProductCategoryUseCaseMock.On("FindAll", mock.Anything).Return(nil, errors.New("error"))

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/product-categories"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := productCategoryApi.GetProductCategories(echoContext)

		getProductCategoryUseCaseMock.AssertExpectations(t)
		getProductCategoryUseCaseMock.AssertCalled(t, "FindAll", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

}
