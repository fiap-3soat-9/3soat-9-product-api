package ingredient

import (
	"3soat-9-product-api/internal/modules/ingredient/usecase/result"
	mocks "3soat-9-product-api/tests/mocks/modules/ingredient/ports/input"
	"3soat-9-product-api/tests/mocks/web/custom_validator"
	"errors"
	"fmt"
	"github.com/go-playground/validator"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

func TestIngredientApi(t *testing.T) {
	t.Run(`should create ingredient`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		ingredientResult := &result.CreateIngredientResult{
			ID:     uuid.New(),
			Name:   "ingredient",
			Amount: 1000,
			Type:   "type",
		}

		requestBody := `{
			"name": "Guaraná Antartica",
			"amount": 800,
			"type": "Bebidas Não Alcoólicas"
		}`

		createIngredientUseCase.On("AddIngredient", mock.Anything, mock.Anything).Return(ingredientResult, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.AddIngredient(echoContext)

		createIngredientUseCase.AssertExpectations(t)
		createIngredientUseCase.AssertCalled(t, "AddIngredient", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when create ingredient validation fails`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		requestBody := `{
			"name": "Guaraná Antartica",
			"amount": 800,
			"type": "Bebidas Não Alcoólicas"
		}`

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorFail{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.AddIngredient(echoContext)

		createIngredientUseCase.AssertExpectations(t)
		createIngredientUseCase.AssertNotCalled(t, "AddIngredient", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return 400 when create ingredient when payload is invalid`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		requestBody := `{
			"name": true,
			"amount": 800,
			"type": "Bebidas Não Alcoólicas"
		}`

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.AddIngredient(echoContext)

		createIngredientUseCase.AssertExpectations(t)
		createIngredientUseCase.AssertNotCalled(t, "AddIngredient", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should find ingredient by number`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		ingredientResult := &result.FindIngredientResult{
			ID:     uuid.New(),
			Number: 1,
			Name:   "ingredient",
			Amount: 1000,
			Type:   "type",
		}

		number := 1

		findIngredientUseCase.On("FindIngredientByNumber", mock.Anything, number).Return(ingredientResult, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/ingredients/%d", number)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(number))

		response := ingredientApi.GetIngredientByNumber(echoContext)

		findIngredientUseCase.AssertExpectations(t)
		findIngredientUseCase.AssertCalled(t, "FindIngredientByNumber", mock.Anything, number)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when find ingredient by invalid number`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		number := "error"

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := fmt.Sprintf("http://localhost:8080/v1/ingredients/%s", number)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(number)

		response := ingredientApi.GetIngredientByNumber(echoContext)

		findIngredientUseCase.AssertExpectations(t)
		findIngredientUseCase.AssertNotCalled(t, "FindIngredientByNumber", mock.Anything, number)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should find all ingredients`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		ingredientResult := result.FindIngredientResult{
			ID:     uuid.New(),
			Number: 1,
			Name:   "ingredient",
			Amount: 1000,
			Type:   "type",
		}

		findIngredientUseCase.On("FindAllIngredients", mock.Anything).Return([]result.FindIngredientResult{ingredientResult}, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.GetIngredients(echoContext)

		findIngredientUseCase.AssertExpectations(t)
		findIngredientUseCase.AssertCalled(t, "FindAllIngredients", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find ingredients`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		findIngredientUseCase.On("FindAllIngredients", mock.Anything).Return(nil, errors.New("some-error"))

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.GetIngredients(echoContext)

		findIngredientUseCase.AssertExpectations(t)
		findIngredientUseCase.AssertCalled(t, "FindAllIngredients", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should find ingredients by type`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		ingredientResult := result.FindIngredientResult{
			ID:     uuid.New(),
			Number: 1,
			Name:   "ingredient",
			Amount: 1000,
			Type:   "type",
		}

		findIngredientUseCase.On("FindIngredientByType", mock.Anything, "type").Return([]result.FindIngredientResult{ingredientResult}, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients?type=type"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.GetIngredients(echoContext)

		findIngredientUseCase.AssertExpectations(t)
		findIngredientUseCase.AssertCalled(t, "FindIngredientByType", mock.Anything, "type")
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find ingredients by type`, func(t *testing.T) {
		createIngredientUseCase := mocks.NewCreateIngredientUseCasePort(t)
		findIngredientUseCase := mocks.NewFindIngredientUseCasePort(t)

		ingredientApi := Api{
			CreateIngredientUseCase: createIngredientUseCase,
			FindIngredientUseCase:   findIngredientUseCase,
		}

		findIngredientUseCase.On("FindIngredientByType", mock.Anything, "type").Return(nil, errors.New("some-error"))

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredients?type=type"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientApi.GetIngredients(echoContext)

		findIngredientUseCase.AssertExpectations(t)
		findIngredientUseCase.AssertCalled(t, "FindIngredientByType", mock.Anything, "type")
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

}
