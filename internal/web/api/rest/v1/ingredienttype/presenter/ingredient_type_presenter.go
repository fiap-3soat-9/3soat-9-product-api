package presenter

import (
	"3soat-9-product-api/internal/modules/ingredient/usecase/result"
	"3soat-9-product-api/internal/web/api/rest/v1/ingredienttype/response"
)

func IngredientTypeResponseFromResult(result []result.IngredientTypeResult) []response.IngredientTypeResponse {
	var ingredientTypeResponse []response.IngredientTypeResponse
	for _, ingredientType := range result {
		ingredientTypeResponse = append(ingredientTypeResponse, response.IngredientTypeResponse{Name: ingredientType.Name})
	}
	return ingredientTypeResponse
}
