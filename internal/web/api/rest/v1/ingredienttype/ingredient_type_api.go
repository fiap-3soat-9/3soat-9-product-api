package ingredienttype

import (
	"3soat-9-product-api/internal/modules/ingredient/ports/input"
	"3soat-9-product-api/internal/web/api/middleware"
	"3soat-9-product-api/internal/web/api/rest/v1/ingredienttype/presenter"
	"github.com/labstack/echo/v4"
	"net/http"
)

type Api struct {
	FindIngredientTypeUseCase input.FindIngredientTypeUseCasePort
}

func (c *Api) RegisterEchoRoutes(e *echo.Echo) {
	group := e.Group("/v1/ingredient-types",
		middleware.GetTraceCallsMiddlewareFunc(),
		middleware.GetLogCallsMiddlewareFunc(),
	)
	group.Add(http.MethodGet, "", c.GetIngredientTypes)
}

// GetIngredientTypes
// @Summary     Get Ingredient types
// @Description Get Ingredient types
// @Produce      json
// @Failure      400 {object} v1.ErrorResponse
// @Failure      401 {object} v1.ErrorResponse
// @Failure      404 {object} v1.ErrorResponse
// @Failure      503 {object} v1.ErrorResponse
// @Success      200 {object} []response.IngredientTypeResponse
// @Router       /v1/ingredient-types [get]
func (c *Api) GetIngredientTypes(ctx echo.Context) error {

	result, err := c.FindIngredientTypeUseCase.FindAll(ctx.Request().Context())

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]any{
			"code":    500,
			"message": err.Error(),
		})
	}

	if result == nil {
		return ctx.JSON(http.StatusNoContent, nil)
	}

	return ctx.JSON(http.StatusOK, presenter.IngredientTypeResponseFromResult(result))

}
