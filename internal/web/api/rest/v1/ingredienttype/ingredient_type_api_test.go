package ingredienttype

import (
	"3soat-9-product-api/internal/modules/ingredient/usecase/result"
	mocks "3soat-9-product-api/tests/mocks/modules/ingredient/ports/input"
	"3soat-9-product-api/tests/mocks/web/custom_validator"
	"errors"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestIngredientTypeApi(t *testing.T) {

	t.Run(`should find all ingredient types`, func(t *testing.T) {
		findIngredientTypeUseCaseMock := mocks.NewFindIngredientTypeUseCasePort(t)

		ingredientTypeApi := Api{
			FindIngredientTypeUseCase: findIngredientTypeUseCaseMock,
		}

		ingredientTypeResult := result.IngredientTypeResult{
			Name: "type",
		}

		findIngredientTypeUseCaseMock.On("FindAll", mock.Anything).Return([]result.IngredientTypeResult{ingredientTypeResult}, nil)

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredient-types"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientTypeApi.GetIngredientTypes(echoContext)

		findIngredientTypeUseCaseMock.AssertExpectations(t)
		findIngredientTypeUseCaseMock.AssertCalled(t, "FindAll", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find all ingredient types`, func(t *testing.T) {
		findIngredientTypeUseCaseMock := mocks.NewFindIngredientTypeUseCasePort(t)

		ingredientTypeApi := Api{
			FindIngredientTypeUseCase: findIngredientTypeUseCaseMock,
		}

		findIngredientTypeUseCaseMock.On("FindAll", mock.Anything).Return(nil, errors.New("error"))

		echoServer := echo.New()
		echoServer.Validator = &custom_validator.ValidatorOK{Validator: validator.New()}
		testUrl := "http://localhost:8080/v1/ingredient-types"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := ingredientTypeApi.GetIngredientTypes(echoContext)

		findIngredientTypeUseCaseMock.AssertExpectations(t)
		findIngredientTypeUseCaseMock.AssertCalled(t, "FindAll", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

}
