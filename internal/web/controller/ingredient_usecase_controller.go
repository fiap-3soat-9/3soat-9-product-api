package controller

import (
	ingredientDatabase "3soat-9-product-api/internal/modules/ingredient/infra/database"
	"3soat-9-product-api/internal/modules/ingredient/ports/input"
	"3soat-9-product-api/internal/modules/ingredient/usecase"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"sync"
)

type IngredientUseCaseController struct {
	CreateIngredientUseCase   input.CreateIngredientUseCasePort
	FindIngredientTypeUseCase input.FindIngredientTypeUseCasePort
	FindIngredientUseCase     input.FindIngredientUseCasePort
}

var (
	ingredientUseCaseControllerInstance *IngredientUseCaseController
	ingredientUseCaseControllerOnce     sync.Once
)

func GetIngredientUseCaseController(readWriteDB, readOnlyDB *gorm.DB) *IngredientUseCaseController {
	ingredientUseCaseControllerOnce.Do(func() {
		ingredientTypePersistence := ingredientDatabase.GetIngredientTypePersistenceGateway(readWriteDB, readOnlyDB, logger.Get())
		ingredientPersistence := ingredientDatabase.GetIngredientPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())

		ingredientUseCaseControllerInstance = &IngredientUseCaseController{
			CreateIngredientUseCase:   usecase.GetCreateIngredientUseCase(ingredientPersistence, ingredientTypePersistence),
			FindIngredientTypeUseCase: usecase.GetIngredientTypeUseCase(ingredientTypePersistence),
			FindIngredientUseCase:     usecase.GetFindIngredientUseCase(ingredientPersistence),
		}
	})

	return ingredientUseCaseControllerInstance
}
