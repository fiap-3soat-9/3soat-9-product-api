package controller

import (
	ingredientDatabase "3soat-9-product-api/internal/modules/ingredient/infra/database"
	productDatabase "3soat-9-product-api/internal/modules/product/infra/database"
	"3soat-9-product-api/internal/modules/product/ports/input"
	"3soat-9-product-api/internal/modules/product/usecase"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"sync"
)

type ProductUseCaseController struct {
	CreateProductUseCase      input.CreateProductUseCasePort
	DeleteProductUseCase      input.DeleteProductUseCasePort
	GetProductCategoryUseCase input.GetProductCategoryUseCasePort
	FindProductUseCase        input.FindProductUseCasePort
	UpdateProductUseCase      input.UpdateProductUseCasePort
}

var (
	productUseCaseControllerInstance *ProductUseCaseController
	productUseCaseControllerOnce     sync.Once
)

func GetProductUseCaseController(readWriteDB, readOnlyDB *gorm.DB) *ProductUseCaseController {
	productUseCaseControllerOnce.Do(func() {
		productPersistence := productDatabase.GetProductPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())
		productCategoryPersistence := productDatabase.GetProductCategoryRepository(readWriteDB, readOnlyDB, logger.Get())
		ingredientPersistence := ingredientDatabase.GetIngredientPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())

		findProductCategoryUseCase := usecase.GetGetProductCategoryUseCase(productCategoryPersistence)
		createProductUseCase := usecase.GetCreateProductUseCase(productPersistence, ingredientPersistence, productCategoryPersistence)
		deleteProductUseCase := usecase.GetDeleteProductUseCase(productPersistence)
		updateProductUseCase := usecase.GetUpdateProductUseCase(productPersistence, ingredientPersistence)
		findProductUseCase := usecase.GetFindProductUseCase(productPersistence)

		productUseCaseControllerInstance = &ProductUseCaseController{
			CreateProductUseCase:      createProductUseCase,
			DeleteProductUseCase:      deleteProductUseCase,
			GetProductCategoryUseCase: findProductCategoryUseCase,
			FindProductUseCase:        findProductUseCase,
			UpdateProductUseCase:      updateProductUseCase,
		}
	})

	return productUseCaseControllerInstance
}
